package com.dooren.b.tm.rest;

import com.dooren.b.am.model.Activity;
import com.dooren.b.am.model.Partition;
import com.dooren.b.cm.model.Customer;
import com.dooren.b.cmn.generics.GenericController;
import com.dooren.b.tm.model.Ticket;
import com.dooren.b.tm.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/ticket")
public class TicketController extends GenericController<Ticket, Integer> {

    private TicketService service;

    @Autowired
    public TicketController(TicketService service) {
        super(service);
        this.service = service;
    }

    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE,
            value = "/dummy"
    )
    @ResponseBody
    public ResponseEntity<Ticket> dummy() {
        Ticket ticket = new Ticket();
        ticket.setActivity(new Activity());
        ticket.setPartition(new Partition());
        ticket.setCustomer(new Customer());
        return new ResponseEntity<Ticket>(ticket, HttpStatus.OK);
    }

}
