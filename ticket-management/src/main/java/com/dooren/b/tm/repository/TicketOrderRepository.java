package com.dooren.b.tm.repository;

import com.dooren.b.cmn.generics.GenericRepository;
import com.dooren.b.tm.model.TicketOrder;
import org.springframework.stereotype.Repository;

@Repository
public interface TicketOrderRepository extends GenericRepository<TicketOrder,Integer> {

}
