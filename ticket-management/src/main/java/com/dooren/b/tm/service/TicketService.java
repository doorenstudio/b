package com.dooren.b.tm.service;

import com.dooren.b.cmn.generics.GenericService;
import com.dooren.b.tm.repository.TicketRepository;
import com.dooren.b.tm.model.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TicketService extends GenericService<Ticket, Integer> {

    private TicketRepository repository;

    @Autowired
    public TicketService(TicketRepository repository) {
        super(repository);
        this.repository=repository;
    }

}

