package com.dooren.b.tm.repository;

import com.dooren.b.cmn.generics.GenericRepository;
import com.dooren.b.tm.model.Ticket;
import org.springframework.stereotype.Repository;

@Repository
public interface TicketRepository extends GenericRepository<Ticket,Integer> {

}
