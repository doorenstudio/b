package com.dooren.b.tm.service;

import com.dooren.b.cmn.generics.GenericService;
import com.dooren.b.tm.model.TicketOrder;
import com.dooren.b.tm.repository.TicketOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TicketOrderService extends GenericService<TicketOrder, Integer> {

    private TicketOrderRepository repository;

    @Autowired
    public TicketOrderService(TicketOrderRepository repository) {
        super(repository);
        this.repository=repository;
    }
}

