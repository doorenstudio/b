package com.dooren.b.tm.constant;

public enum TicketStatus {
    BOOKED,
    PURCHASED,
    USED,
    REFUNDED_BY_CUSTOMER,
    REFUNDED_BY_PROVIDER,
}
