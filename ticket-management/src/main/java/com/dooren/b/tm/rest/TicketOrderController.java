package com.dooren.b.tm.rest;

import com.dooren.b.cmn.generics.GenericController;
import com.dooren.b.tm.model.Ticket;
import com.dooren.b.tm.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/ticket.order")
public class TicketOrderController extends GenericController<Ticket, Integer> {

    private TicketService service;

    @Autowired
    public TicketOrderController(TicketService service) {
        super(service);
        this.service = service;
    }

}
