package com.dooren.b.tm.model;

import com.dooren.b.cmn.generics.AuditableEntity;
import com.dooren.b.tm.constant.TicketStatus;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "t_ticket_orders")
public class TicketOrder extends AuditableEntity {
    @Id
    @Column(name = "ticket_order_id", columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ticket_id", columnDefinition = "int")
    private Ticket ticket;

    @Column(name = "ticket_status")
    private TicketStatus ticketStatus;

    @Column(name = "date")
    private Date date;

    @Column(name = "remarks")
    private String remarks;

    public TicketOrder() {
    }

    public TicketOrder(Ticket ticket, TicketStatus ticketStatus, Date date, String remarks) {
        this.ticket = ticket;
        this.ticketStatus = ticketStatus;
        this.date = date;
        this.remarks = remarks;
    }

    public int getId() {
        return id;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public TicketStatus getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(TicketStatus ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
