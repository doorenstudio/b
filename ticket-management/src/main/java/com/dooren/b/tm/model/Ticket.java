package com.dooren.b.tm.model;

import com.dooren.b.am.model.Activity;
import com.dooren.b.am.model.Partition;
import com.dooren.b.cm.model.Customer;
import com.dooren.b.cmn.generics.AuditableEntity;
import com.dooren.b.tm.constant.TicketStatus;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "m_tickets")
public class Ticket extends AuditableEntity {
    @Id
    @Column(name = "ticket_id", columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(name = "ticket_code", unique = true)
    private String ticketCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "activity_id", columnDefinition = "int")
    private Activity activity;

    @Column(name = "activity_date")
    private Date activityDate;

    private Partition partition;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id", columnDefinition = "int")
    private Customer customer;

    @Column(name = "ticket_status")
    private TicketStatus ticketStatus;

    public Ticket() {
    }

    public Ticket(String ticketCode, Activity activity, Date activityDate, Partition partition, Customer customer, TicketStatus ticketStatus) {
        this.ticketCode = ticketCode;
        this.activity = activity;
        this.activityDate = activityDate;
        this.partition = partition;
        this.customer = customer;
        this.ticketStatus = ticketStatus;
    }

    public int getId() {
        return id;
    }

    public TicketStatus getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(TicketStatus ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    public String getTicketCode() {
        return ticketCode;
    }

    public void setTicketCode(String ticketCode) {
        this.ticketCode = ticketCode;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public Date getActivityDate() {
        return activityDate;
    }

    public void setActivityDate(Date activityDate) {
        this.activityDate = activityDate;
    }

    public Partition getPartition() {
        return partition;
    }

    public void setPartition(Partition partition) {
        this.partition = partition;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
