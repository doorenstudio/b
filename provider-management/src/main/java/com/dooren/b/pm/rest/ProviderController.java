package com.dooren.b.pm.rest;

import com.dooren.b.cmn.generics.GenericController;
import com.dooren.b.pm.model.Provider;
import com.dooren.b.pm.service.ProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/provider")
public class ProviderController extends GenericController<Provider, Integer> {

    private ProviderService service;

    @Autowired
    public ProviderController(ProviderService service) {
        super(service);
        this.service = service;
    }

    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE,
            value = "/dummy"
    )
    @ResponseBody
    public ResponseEntity<Provider> dummy() {
        return new ResponseEntity<Provider>(new Provider(), HttpStatus.OK);
    }

}
