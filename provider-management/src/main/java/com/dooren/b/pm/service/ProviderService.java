package com.dooren.b.pm.service;

import com.dooren.b.cmn.generics.GenericService;
import com.dooren.b.pm.repository.ProviderRepository;
import com.dooren.b.pm.model.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProviderService extends GenericService<Provider, Integer> {

    private ProviderRepository repository;

    @Autowired
    public ProviderService(ProviderRepository repository) {
        super(repository);
        this.repository=repository;
    }

}

