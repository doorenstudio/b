package com.dooren.b.pm.model;

import com.dooren.b.cmn.generics.AuditableEntity;

import javax.persistence.*;

@Entity
@Table(name = "m_providers")
public class Provider extends AuditableEntity {

    @Id
    @Column(name = "provider_id", columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(name = "provider_code", unique = true)
    private String providerCode;

    @Column(name = "provider_name")
    private String providerName;

    @Column(name = "description")
    private String description;

    @Column(name = "owner_name")
    private String ownerName;

    @Column(name = "email")
    private String email;

    @Column(name = "phone_1")
    private String phone1;

    @Column(name = "phone_2")
    private String phone2;

    @Column(name = "website")
    private String website;

    @Column(name = "active")
    private boolean active;

    public Provider() {
    }

    public Provider(String providerCode, String providerName, String description, String ownerName, String email, String phone1, String phone2, String website, boolean active) {
        this.providerCode = providerCode;
        this.providerName = providerName;
        this.description = description;
        this.ownerName = ownerName;
        this.email = email;
        this.phone1 = phone1;
        this.phone2 = phone2;
        this.website = website;
        this.active = active;
    }

    public int getId() {
        return id;
    }

    public String getProviderCode() {
        return providerCode;
    }

    public void setProviderCode(String providerCode) {
        this.providerCode = providerCode;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
