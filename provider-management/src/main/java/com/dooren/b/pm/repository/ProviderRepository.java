package com.dooren.b.pm.repository;

import com.dooren.b.cmn.generics.GenericRepository;
import com.dooren.b.pm.model.Provider;
import org.springframework.stereotype.Repository;

@Repository
public interface ProviderRepository extends GenericRepository<Provider,Integer> {

}
