package com.dooren.b.um.service;

import com.dooren.b.cmn.generics.GenericService;
import com.dooren.b.um.repository.UserRepository;
import com.dooren.b.um.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService extends GenericService<User, Integer> {

    private UserRepository repository;

    @Autowired
    public UserService(UserRepository repository) {
        super(repository);
        this.repository=repository;
    }

}

