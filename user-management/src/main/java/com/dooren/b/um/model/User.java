package com.dooren.b.um.model;

import com.dooren.b.cmn.generics.AuditableEntity;
import com.dooren.b.um.constant.RoleType;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "m_users")
public class User extends AuditableEntity {

    @Id
    @Column(name = "user_id", columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(name = "username", unique = true, nullable = false)
    private String userName;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "is_enabled")
    private boolean enabled;

    @Column(name = "is_active")
    private boolean active;

    @Column(name = "email")
    private String email;

    @Column(name = "login_attempt")
    private Integer loginAttempt;

    @Column(name = "disable_reason")
    private String disableReason;

    @Temporal(TemporalType.DATE)
    @Column(name = "password_expire_date")
    private Date passwordExpireDate;

    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private RoleType roleType;

    public User() {

    }

    public User(String userName, String password, boolean active, boolean enabled, String email, Integer loginAttempt, String disableReason, Date passwordExpireDate, RoleType roleType) {
        this.userName = userName;
        this.password = password;
        this.active = active;
        this.enabled = enabled;
        this.email = email;
        this.loginAttempt = loginAttempt;
        this.disableReason = disableReason;
        this.passwordExpireDate = passwordExpireDate;
        this.roleType = roleType;
    }

    public int getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getLoginAttempt() {
        return loginAttempt;
    }

    public void setLoginAttempt(Integer loginAttempt) {
        this.loginAttempt = loginAttempt;
    }

    public String getDisableReason() {
        return disableReason;
    }

    public void setDisableReason(String disableReason) {
        this.disableReason = disableReason;
    }

    public Date getPasswordExpireDate() {
        return passwordExpireDate;
    }

    public void setPasswordExpireDate(Date passwordExpireDate) {
        this.passwordExpireDate = passwordExpireDate;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }
}
