package com.dooren.b.um.constant;

/**
 * Created by nyccy.pribawa on 18/10/2015.
 */
public enum RoleType {
    ADMIN,
    PROVIDER,
    CUSTOMER
}
