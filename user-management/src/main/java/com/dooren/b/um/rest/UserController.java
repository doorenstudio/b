package com.dooren.b.um.rest;

import com.dooren.b.cmn.generics.GenericController;
import com.dooren.b.um.model.User;
import com.dooren.b.um.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/user")
public class UserController extends GenericController<User, Integer> {

    private UserService service;

    @Autowired
    public UserController(UserService service) {
        super(service);
        this.service = service;
    }

    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE,
            value = "/dummy"
    )
    @ResponseBody
    public ResponseEntity<User> dummy() {
        return new ResponseEntity<User>(new User(), HttpStatus.OK);
    }

    @RequestMapping(
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE,
            value = "/changePassword/{id}/{password}"
    )
    @ResponseBody
    public ResponseEntity<User> changePassword(@PathVariable("id")Integer id, @PathVariable("password")String password) {
        User user = service.findById(id);
        user.setPassword(password);
        User userupdate = service.update(user, id);
        return new ResponseEntity<User>(userupdate, HttpStatus.OK);

    }

}
