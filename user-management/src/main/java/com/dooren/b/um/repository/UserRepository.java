package com.dooren.b.um.repository;

import com.dooren.b.cmn.generics.GenericRepository;
import com.dooren.b.um.model.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends GenericRepository<User,Integer> {

    public User findByUserName(String username);
}
