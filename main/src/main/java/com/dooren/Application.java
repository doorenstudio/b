package com.dooren;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Arrays;

@SpringBootApplication
public class Application {

    private static ConfigurableApplicationContext context = null;

    public static void main(String... args) {
        getApplicationContext(args);
    }

    public static ConfigurableApplicationContext getApplicationContext(String... args) {
        if (context == null) {
            context = SpringApplication.run(Application.class, args);
            System.out.println("All beans from spring boot :");
            String[] beans = context.getBeanDefinitionNames();
            Arrays.sort(beans);
            for (String s : beans) {
                System.out.println("bean : " + s);
            }

        }
        return context;
    }

}

