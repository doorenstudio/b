package com.dooren.b.am.service;

import com.dooren.b.am.model.Activity;
import com.dooren.b.am.repository.ActivityRepository;
import com.dooren.b.cmn.generics.GenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivityService extends GenericService<Activity, Integer> {

    private ActivityRepository repository;

    @Autowired
    public ActivityService(ActivityRepository repository) {
        super(repository);
        this.repository = repository;
    }

    public List<Activity> findByActivityTypeLookupKey(String activityType) {
        return repository.findByActivityType(activityType.toUpperCase());
    }

    public List<Activity> findByOpeningDays(String day) {
        return repository.findByOpeningDaysLikeIgnoreCase(day);
    }

    public List<Activity> findByTypeAndDay(String activityType, String day) {
        return repository.findByActivityTypeAndOpeningDaysLikeIgnoreCase(activityType, day);
    }
    public List<Activity> findByProvinceNameOrCityName(String name) {
        return repository.findByAddressProvinceNameLikeIgnoreCaseOrAddressCityNameLikeIgnoreCase(name, name);
    }

    public List<Activity> findByTypeAndLocation(String type, String location) {
        return repository.findByActivityTypeAndAddressProvinceNameLikeIgnoreCaseOrAddressCityNameLikeIgnoreCase(type, location, location);
    }

}

