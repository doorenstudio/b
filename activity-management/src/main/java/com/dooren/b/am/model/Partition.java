package com.dooren.b.am.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.Date;

@Embeddable
public class Partition {

    @Column(name = "partition_id")
    private String partitionId;

    @Column(name = "partition_no")
    private int partitionNo;

    @Column(name = "quota")
    private int quota;

    @Column(name = "start_hour")
    private Date startHour;

    @Column(name = "weather_prediction")
    private String weatherPrediction;

    public Partition() {
    }

    public Partition(String partitionId, int partitionNo, int quota, Date startHour, String weatherPrediction) {
        this.partitionId = partitionId;
        this.partitionNo = partitionNo;
        this.quota = quota;
        this.startHour = startHour;
        this.weatherPrediction = weatherPrediction;
    }

    public int getPartitionNo() {
        return partitionNo;
    }

    public void setPartitionNo(int partitionNo) {
        this.partitionNo = partitionNo;
    }

    public String getWeatherPrediction() {
        return weatherPrediction;
    }

    public void setWeatherPrediction(String weatherPrediction) {
        this.weatherPrediction = weatherPrediction;
    }

    public String getPartitionId() {
        return partitionId;
    }

    public void setPartitionId(String partitionId) {
        this.partitionId = partitionId;
    }

    public int getQuota() {
        return quota;
    }

    public void setQuota(int quota) {
        this.quota = quota;
    }

    public Date getStartHour() {
        return startHour;
    }

    public void setStartHour(Date startHour) {
        this.startHour = startHour;
    }
}
