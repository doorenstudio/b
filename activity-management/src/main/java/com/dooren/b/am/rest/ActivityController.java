package com.dooren.b.am.rest;

import com.dooren.b.am.model.Activity;
import com.dooren.b.am.model.Address;
import com.dooren.b.am.model.Partition;
import com.dooren.b.am.service.ActivityService;
import com.dooren.b.cmn.generics.GenericController;
import com.dooren.b.cmn.model.Region;
import com.dooren.b.pm.model.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/activity")
public class ActivityController extends GenericController<Activity, Integer> {

    private ActivityService service;

    @Autowired
    public ActivityController(ActivityService service) {
        super(service);
        this.service = service;
    }

    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE,
            value = "/dummy"
    )
    @ResponseBody
    public ResponseEntity<Activity> dummy() {
        List<Partition> p = new ArrayList<Partition>();
        p.add(new Partition());
        List<String> foto= new ArrayList<String>();
        foto.add("foto.img");
        Address add = new Address();
        add.setCity(new Region());
        add.setDistrict(new Region());
        add.setProvince(new Region());
        add.setVillage(new Region());
        Activity a = new Activity();
        a.setPartitions(p);
        a.setPhotoUrls(foto);
        a.setProvider(new Provider());
        a.setAddress(add);
        return new ResponseEntity<Activity>(a, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/type/{type}")
    @ResponseBody
    public ResponseEntity<List<Activity>> byType(@PathVariable("type") String type) {
        List<Activity> entities = service.findByActivityTypeLookupKey(type);
        return new ResponseEntity<List<Activity>>(entities, HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/day/{day}")
    @ResponseBody
    public ResponseEntity<List<Activity>> byDay(@PathVariable("day") String day) {
        List<Activity> entities = service.findByOpeningDays(day);
        return new ResponseEntity<List<Activity>>(entities, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/typeAndDay/{type}/{day}/")
    @ResponseBody
    public ResponseEntity<List<Activity>> byTypeAndDay(@PathVariable("type") String type, @PathVariable("day") String day) {
        List<Activity> entities = service.findByTypeAndDay(type, day);
        return new ResponseEntity<List<Activity>>(entities, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/typeAndLocation/{type}/{location}")
    @ResponseBody
    public ResponseEntity<List<Activity>> byTypeAndLocation(@PathVariable("type") String type, @PathVariable("location") String location) {
        List<Activity> entities = service.findByTypeAndLocation(type, location);
        return new ResponseEntity<List<Activity>>(entities, HttpStatus.OK);
    }



}
