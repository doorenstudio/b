package com.dooren.b.am.model;

import com.dooren.b.cmn.model.Region;

import javax.persistence.*;

/**
 * Created by nyccy.pribawa on 14/11/2015.
 */

@Embeddable
public class Address {
    @Column(name="street")
    private String street;

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="province_id", columnDefinition = "int")
    private Region province;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="city_id", columnDefinition = "int")
    private Region city;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="district_id", columnDefinition = "int")
    private Region district;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="village_id", columnDefinition = "int")
    private Region village;

    @Column(name="postal_code")
    private String postalCode;

    @Column(name="latitude")
    private String latitude;

    @Column(name="longitude")
    private String longitude;

    public Address() {
    }

    public Address(String street, Region province, Region city, Region district, Region village, String postalCode, String latitude, String longitude) {
        this.street = street;
        this.province = province;
        this.city = city;
        this.district = district;
        this.village = village;
        this.postalCode = postalCode;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Region getProvince() {
        return province;
    }

    public void setProvince(Region province) {
        this.province = province;
    }

    public Region getCity() {
        return city;
    }

    public void setCity(Region city) {
        this.city = city;
    }

    public Region getDistrict() {
        return district;
    }

    public void setDistrict(Region district) {
        this.district = district;
    }

    public Region getVillage() {
        return village;
    }

    public void setVillage(Region village) {
        this.village = village;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
