package com.dooren.b.am.model;

import com.dooren.b.cmn.generics.AuditableEntity;
import com.dooren.b.pm.model.Provider;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "m_activities")
public class Activity extends AuditableEntity {
    @Id
    @Column(name = "activity_id", columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name="activity_name")
    private String activityName;

    @Column(name="description")
    private String description;

    @Column(name="activity_type")
    @Enumerated(EnumType.STRING)
    private ActivityType activityType;

    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "m_activity_photos", joinColumns = {@JoinColumn(name = "activity_id")})
    private List<String> photoUrls;

    private Address address;

    @Column(name="opening_days")
    private String openingDays;

    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "m_activity_partitions", joinColumns = {@JoinColumn(name = "activity_id")})
    private List<Partition> partitions;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="provider_id", columnDefinition = "int")
    private Provider provider;

    @Column(name = "rating")
    private long rating;

    public Activity() {
    }

    public Activity(String activityName, String description, ActivityType activityType, List<String> photoUrls, Address address, String openingDays, List<Partition> partitions, Provider provider, long rating) {
        this.activityName = activityName;
        this.description = description;
        this.activityType = activityType;
        this.photoUrls = photoUrls;
        this.address = address;
        this.openingDays = openingDays;
        this.partitions = partitions;
        this.provider = provider;
        this.rating = rating;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ActivityType getActivityType() {
        return activityType;
    }

    public void setActivityType(ActivityType activityType) {
        this.activityType = activityType;
    }

    public List<String> getPhotoUrls() {
        return photoUrls;
    }

    public void setPhotoUrls(List<String> photoUrls) {
        this.photoUrls = photoUrls;
    }

    public String getOpeningDays() {
        return openingDays;
    }

    public void setOpeningDays(String openingDays) {
        this.openingDays = openingDays;
    }

    public List<Partition> getPartitions() {
        return partitions;
    }

    public void setPartitions(List<Partition> partitions) {
        this.partitions = partitions;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public long getRating() {
        return rating;
    }

    public void setRating(long rating) {
        this.rating = rating;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
