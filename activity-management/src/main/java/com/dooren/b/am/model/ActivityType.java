package com.dooren.b.am.model;

public enum ActivityType {
    CAVING,
    CLIMBING,
    DIVING,
    OUTBOND,
    PAINTBALL,
    PARACEILING,
    PARAGLIDING,
    PARALAYANG,
    TUBING
}
