package com.dooren.b.am.repository;

import com.dooren.b.am.model.Activity;
import com.dooren.b.cmn.generics.GenericRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActivityRepository extends GenericRepository<Activity,Integer> {
    public List<Activity> findByActivityType(String activityType);
    public List<Activity> findByAddressProvinceNameLikeIgnoreCaseOrAddressCityNameLikeIgnoreCase(String provinceName, String cityName);
    public List<Activity> findByOpeningDaysLikeIgnoreCase(String day);
    public List<Activity> findByActivityTypeAndOpeningDaysLikeIgnoreCase(String activityType, String day);
    public List<Activity> findByActivityTypeAndAddressProvinceNameLikeIgnoreCaseOrAddressCityNameLikeIgnoreCase(String activityType, String provinceName, String cityName);
}
