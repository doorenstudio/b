package com.dooren.b.cm.repository;

import com.dooren.b.cm.model.Customer;
import com.dooren.b.cmn.generics.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends GenericRepository<Customer,Integer> {

}
