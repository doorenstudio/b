package com.dooren.b.cm.service;

import com.dooren.b.cm.repository.CustomerRepository;
import com.dooren.b.cm.model.Customer;
import com.dooren.b.cmn.generics.GenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService extends GenericService<Customer, Integer> {

    private CustomerRepository repository;

    @Autowired
    public CustomerService(CustomerRepository repository) {
        super(repository);
        this.repository=repository;
    }

}

