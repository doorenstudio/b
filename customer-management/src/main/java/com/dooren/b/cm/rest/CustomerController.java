package com.dooren.b.cm.rest;

import com.dooren.b.cm.model.Customer;
import com.dooren.b.cm.service.CustomerService;
import com.dooren.b.cmn.generics.GenericController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/customer")
public class CustomerController extends GenericController<Customer, Integer> {

    private CustomerService service;

    @Autowired
    public CustomerController(CustomerService service) {
        super(service);
        this.service = service;
    }

    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE,
            value = "/dummy"
    )
    @ResponseBody
    public ResponseEntity<Customer> dummy() {
        return new ResponseEntity<Customer>(new Customer(), HttpStatus.OK);
    }

}
