package com.dooren.b.cmn.util;

public enum ComparisonOperator
{
    EQUAL("==", "Equal To"), NOT_EQUAL("!=", "Not Equal"),
    LESS_THAN("<", "Less Than"), LESS_THAN_OR_EQUAL_TO("<=", "Less Than or Equal To"),
    GREATER_THAN(">", "Greater Than"), GREATER_THAN_OR_EQUAL_TO(">=", "Greater Than or Equal To");
    
    private String code;
    private String name;
    
    private ComparisonOperator(String code, String name)
    {
        this.code = code;
        this.name = name;
    }

    public String getCode()
    {
        return code;
    }

    public String getName()
    {
        return name;
    }
}
