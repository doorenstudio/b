package com.dooren.b.cmn.util;

import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by rnpratama on 10/9/2015.
 * this class initialize lazy fetch type
 */
@Transactional
@Service
public class RepositoryHelper {

    private final Integer DEFAULT_DEPTH=1;

    @PersistenceContext
    private EntityManager entityManager;

    public void init(Object entity, int depth){
        JpaUtils.initialize(entityManager,entity,depth);
    }

    public void init(Object o, List<String> attrs){
        JpaUtils.initialize(entityManager,o,DEFAULT_DEPTH,attrs);
    }

}
