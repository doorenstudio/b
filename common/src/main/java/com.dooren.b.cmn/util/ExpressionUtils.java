package com.dooren.b.cmn.util;

import com.dooren.b.cmn.exception.ExpressionUtilsException;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Daniel Joi Partogi Hutapea
 */
public class ExpressionUtils
{
    private static final String IN = "IN";
    
    private ExpressionUtils()
    {
    }
    
    public static boolean isIn(String value)
    {
        return isExpression(value, IN);
    }
    
    private static boolean isExpression(String value, String expression)
    {
        boolean result = false;
        
        if(value!=null && expression!=null)
        {
            String valueLowerCase = value.toLowerCase();
            String expressionLowerCase = expression.toLowerCase();
            result = valueLowerCase.startsWith(expressionLowerCase+"(") && valueLowerCase.endsWith(")");
        }
        
        return result;
    }
    
    public static List inValues(String value)
    {
        if(!isIn(value))
        {
            throw new ExpressionUtilsException(String.format("Value is not 'IN' expression."));
        }
        
        List result = new ArrayList();
        
        if(value!=null)
        {
            /**
             * Remove expression from real value.
             * 
             * Exp: IN(abc)
             * After expression removed: abc
             */
            int startExpressionLastIndex = IN.length()+1;
            value = value.substring(startExpressionLastIndex);
            value = value.substring(0,value.length()-1); // Remove last ')' character.
            
            String[] values = value.split(",");
            
            for(String temp : values)
            {
                result.add(temp.trim());
            }
        }
        
        return result;
    }
}