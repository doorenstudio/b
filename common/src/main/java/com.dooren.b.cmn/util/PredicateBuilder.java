package com.dooren.b.cmn.util;



import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/* unchecked unsafe */
public class PredicateBuilder<T>
{
    public static int MATCH_MODE_EXACT = 1;
    public static int MATCH_MODE_START = 2;
    public static int MATCH_MODE_END = 3;
    public static int MATCH_MODE_ANYWHERE = 4;
    
    private Root root;
    private CriteriaQuery cq;
    private CriteriaBuilder cb;
    
    private List<Predicate> listOfPredicate = new ArrayList<Predicate>();
    private List<Predicate> listOfOrPredicate = new ArrayList<Predicate>();
    
    public PredicateBuilder(Root root, CriteriaQuery cq, CriteriaBuilder cb)
    {
        this.root = root;
        this.cq = cq;
        this.cb = cb;
    }
    
    public void addEqualPredicate(From from, String propertyName, Object value)
    {
        if(!isBlank(value))
        {
            listOfPredicate.add(cb.equal(from.<String>get(propertyName), value));
        }
    }
    
    public void addEqualOrPredicate(From from, String propertyName, Object value)
    {
        if(!isBlank(value))
        {
            listOfOrPredicate.add(cb.equal(from.<String>get(propertyName), value));
        }
    }
    

    public void addEqualTruncDatePredicate(String propertyName, Date value)
    {
        if(!isBlank(value))
        {
            listOfPredicate.add(cb.equal(cb.function("DATE", Date.class, root.<String>get(propertyName)), cb.function("DATE", Date.class, cb.literal(value))));
        }
    }
    

    public void addNotEqualPredicate(String propertyName, Object value)
    {
        if(!isBlank(value))
        {
            listOfPredicate.add(cb.notEqual(root.<String>get(propertyName), value));
        }
    }
    

    public void addLikePredicate(String propertyName, Integer value, int matchMode)
    {
        if(value!=null)
        {
            addLikePredicate(propertyName, String.valueOf(value), matchMode);
        }
    }
    
    public void addLikePredicate(String propertyName, String value, int matchMode)
    {
        if(!isBlank(value))
        {
            value = buildMatchMode(value, matchMode);
            listOfPredicate.add(cb.like(root.<String>get(propertyName).as(String.class), value)); //.as(String.class) make Integer column convert to String thus we can use "LIKE" operator for Integer Column.
        }
    }
    
    public void addLikeIgnoreCasePredicate(String propertyName, String value)
    {
        addLikeIgnoreCasePredicate(propertyName, value, MATCH_MODE_EXACT);
    }
    
    public void addLikeIgnoreCasePredicate(String propertyName, String value, int matchMode)
    {
        if(!isBlank(value))
        {
            value = buildMatchMode(value, matchMode);
            listOfPredicate.add(cb.like(cb.upper(root.<String>get(propertyName).as(String.class)), value.toUpperCase())); //.as(String.class) make Integer column convert to String thus we can use "LIKE" operator for Integer Column.
        }
    }
    
    public void addLessThanPredicate(String propertyName, Comparable value)
    {
        if(!isBlank(value))
        {
            listOfPredicate.add(cb.lessThan(root.<String>get(propertyName), value));
        }
    }
    
    public void addLessThanOrEqualToPredicate(String propertyName, Comparable value)
    {
        if(!isBlank(value))
        {
            listOfPredicate.add(cb.lessThanOrEqualTo(root.<String>get(propertyName), value));
        }
    }
    

    public void addGreaterThanPredicate(String propertyName, Comparable value)
    {
        if(!isBlank(value))
        {
            listOfPredicate.add(cb.greaterThan(root.<String>get(propertyName), value));
        }
    }
    
    public void addGreaterThanOrEqualToPredicate(String propertyName, Comparable value)
    {
        if(!isBlank(value))
        {
            listOfPredicate.add(cb.greaterThanOrEqualTo(root.<String>get(propertyName), value));
        }
    }

    public void addBetweenTruncDatePredicate(String propertyName, Date since, Date until)
    {
        if(!isBlank(since) && !isBlank(until))
        {
            listOfPredicate.add(cb.between(cb.function("TRUNC", Date.class, root.<String>get(propertyName)), cb.function("TRUNC", Date.class, cb.literal(since)), cb.function("TRUNC", Date.class, cb.literal(until))));
        }
    }
    
    


    private String buildMatchMode(String value, int matchMode)
    {
        if(matchMode==MATCH_MODE_EXACT)
        {
            return value;
        }
        
        if(matchMode==MATCH_MODE_ANYWHERE)
        {
            value = '%'+value+'%';
        }
        else if(matchMode==MATCH_MODE_START)
        {
            value = '%'+value;
        }
        else if(matchMode==MATCH_MODE_END)
        {
            value = value+'%';
        }
        
        return value;
    }
    
    public Predicate[] getPredicates()
    {
        if(listOfPredicate.isEmpty())
        {
            return null;
        }
        else
        {
            Predicate[] arrayOfPredicate = new Predicate[listOfPredicate.size()];
            return listOfPredicate.toArray(arrayOfPredicate);
        }
    }
    
    public Predicate getAndPredicate()
    {
        if(listOfPredicate.isEmpty())
        {
            return null;
        }
        else
        {
            Predicate[] arrayOfPredicate = new Predicate[listOfPredicate.size()];
            arrayOfPredicate = listOfPredicate.toArray(arrayOfPredicate);
            return cb.and(arrayOfPredicate);
        }
    }
    
    public Predicate getOrPredicate()
    {
        if(listOfOrPredicate.isEmpty())
        {
            return null;
        }
        else
        {
            Predicate[] arrayOfPredicate = new Predicate[listOfOrPredicate.size()];
            arrayOfPredicate = listOfOrPredicate.toArray(arrayOfPredicate);
            return cb.or(arrayOfPredicate);
        }
    }
    
    public boolean isEmpty()
    {
        return listOfPredicate.isEmpty();
    }
    
    private boolean isBlank(Object value)
    {
        if(value==null)
        {
            return true;
        }
        
        if(value instanceof String)
        {
            return ((String)value).isEmpty();
        }
        
        return false;
    }
}
