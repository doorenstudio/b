package com.dooren.b.cmn.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Helper to translate filtering via HTTP Request into Spring's JPA
 * Specification.
 *
 * Basic behavior is as following:
 *
 *
 *
 * filtering :
 *
 * via filter query param, e.g.:
 * /user?filter=fName::john|lName::Doe|createdDate::2014-01-01..2014-01-31
 *
 * via direct query parameters, e.g.:
 * /user?fName=John&lName=Doe&createdDate?2014-01-01..2014-01-31 but note that
 * the field name used must not in the reserved keywords. Use filter query
 * parameter if otherwise.
 *
 * Like filtering could be done via '*' (asterisk) instead of '%' percent.
 *
 *
 * pagination :
 *
 * via HTTP Header, e.g.: Range=items=0-11 <br />
 * or via offset & limit query parameters <br />
 * e.g.: /user?offset=0&limit=12
 *
 * note the difference behavior between query param & http header when defining
 * pagination.
 *
 *
 *
 * sorting :
 *
 * via filter query param, e.g.:<br />
 * /user?sort=+fName,-lName <br />
 * note the + notation refer to ascending and the - notation refer to
 * descending.
 *
 * @author fkurniadi
 */
public class RestQueryHelper
{
    private static final Logger LOGGER = LoggerFactory.getLogger(RestQueryHelper.class);

    public static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ"); // Exp. 2013-12-31T23:59:59+0700
    public static final SimpleDateFormat SDF_WITHOUT_TIMEZONE = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // Exp. 2013-12-31 23:59:59

    private static final String HTTP_HEADER_USER_TIME_OFFSET = "user-time-offset";
    private static final String HTTP_HEADER_RANGE = "range"; //- pagination
    private static final String QUERY_OFFSET = "offset"; //-- pagination
    private static final String QUERY_LIMIT = "limit"; //-- pagination
    private static final String QUERY_FIELDS = "fields"; //-- limit selected fields
    private static final String QUERY_SORT = "sort"; //-- sorting
    private static final String QUERY_SMART_FILTER = "smartFilter";
    private static final String QUERY_SMART_FILTER_FIELDS = "smartFilterFields";
    private static final String QUERY_FILTER = "filter"; //-- data filtering (where).

    /**
     * List of reserved query parameters. If any of the field name similar these
     * list, use filter query param instead.
     */
    private static final List<String> RESERVED_QUERY_PARAMETER = Arrays.asList(new String[]
            {
                    QUERY_OFFSET, QUERY_LIMIT, QUERY_FIELDS,
                    QUERY_FILTER, QUERY_SMART_FILTER, QUERY_SMART_FILTER_FIELDS, QUERY_SORT,
                    "oauth_signature", "oauth_nonce", "oauth_version", "oauth_consumer_key",
                    "oauth_signature_method", "oauth_timestamp", "oauth_access_token"
            });

    private final Map<String, String> httpHeaders;
    private final Map<String, String> queryParams;

    public RestQueryHelper(Map<String, String> httpHeaders, Map<String, String> queryParameters)
    {
        this.httpHeaders = httpHeaders;
        this.queryParams = queryParameters;
    }

    public Pageable getRequestRangeAndSort() {
        int from = 0, to = 23; //-- default two dozen

        if (httpHeaders != null) {   //-- Range: items=0-24
            String range = httpHeaders.get(HTTP_HEADER_RANGE);
            if (range != null && range.length() > 0) {
                LOGGER.debug("range header : {}", range);
                try {
                    range = range.trim().toLowerCase();
                    range = range.substring(("items=").length());
                    String[] value = range.split("-");
                    if (value.length >= 2) {
                        from = Integer.parseInt(value[0]);
                        to = Integer.parseInt(value[1]);
                    }
                } catch (NumberFormatException e) {
                    LOGGER.error("Failed to get range header.", e);
                }
            }
        }

        //-- override range header
        if (queryParams != null) {   //-- /dogs?offset=0&limit=25
            String offset = queryParams.get(QUERY_OFFSET);
            String limit = queryParams.get(QUERY_LIMIT);
            if (offset != null && limit != null && offset.length() > 0 && limit.length() > 0) {
                LOGGER.debug("offset : {}, limit : {}", offset, limit);
                try {
                    int iOffset = Integer.parseInt(offset.trim());
                    int iLimit = Integer.parseInt(limit.trim());
                    from = iOffset;
                    to = (iOffset + iLimit - 1);
                } catch (NumberFormatException e) {
                    LOGGER.error("Failed to get offset & limit query.", e);
                }
            }
        }

        int rowPerPage = (to - from) + 1;
        int pageIndex = ((to + 1) / rowPerPage) - 1;
        LOGGER.trace("PAGE : {}, ROW/PAGE: {}", pageIndex, rowPerPage);

        Sort sort = null;
        if (queryParams != null) {   //-- /dog?sort=last_name,first_name,-hire_date
            String sortQuery = queryParams.get(QUERY_SORT);
            if (sortQuery != null && sortQuery.trim().length() > 0) {
                LOGGER.debug("sort : {}", sortQuery);
                try {
                    List<Order> orders = new ArrayList<Order>();
                    //-- split by "," char.
                    String[] sorts = sortQuery.split(",");
                    for (String s : sorts) {
                        String tmp = s.trim();
                        if (tmp.length() == 0) {
                            continue;
                        }
                        if (tmp.startsWith("-")) {
                            tmp = tmp.substring(1);
                            orders.add(new Order(Sort.Direction.DESC, tmp));
                        } else if (tmp.startsWith("+")) {
                            tmp = tmp.substring(1);
                            orders.add(new Order(Sort.Direction.ASC, tmp));
                        } else {
                            orders.add(new Order(Sort.Direction.ASC, tmp));
                        }
                    }
                    sort = new Sort(orders);
                } catch (Exception e) {
                    LOGGER.error("Failed to get sort query.", e);
                }
            }
        }

        return new PageRequest(pageIndex, rowPerPage, sort);
    }

    private static <T> Predicate getSimplePredicate(Root<T> root, CriteriaQuery<?> cq, CriteriaBuilder cb, String fieldName, String value) throws Exception {
        String[] listOfPropertyName = fieldName.split("[.]");
        Path path = root;

        for(String temp : listOfPropertyName)
        {
            path = path.get(temp);
        }

        Class selectedClazz = path.getJavaType();
        Path selectedPath = path;

        if(ExpressionUtils.isIn(value))
        {
            return selectedPath.in(ExpressionUtils.inValues(value));
        }
        else if (selectedClazz.equals(String.class)) {
            return cb.like(cb.lower(selectedPath.as(String.class)), value.toLowerCase().replaceAll("\\*", "%"));
        } else if (selectedClazz.equals(boolean.class) || selectedClazz.equals(Boolean.class)) {
            return cb.equal(selectedPath.as(Boolean.class), Boolean.valueOf(value));
        } else if (selectedClazz.equals(float.class) || selectedClazz.equals(Float.class)) {
            return cb.equal(selectedPath.as(Float.class), Float.valueOf(value));
        } else if (selectedClazz.equals(double.class) || selectedClazz.equals(Double.class)) {
            return cb.equal(selectedPath.as(Double.class), Double.valueOf(value));
        } else if (selectedClazz.equals(int.class) || selectedClazz.equals(Integer.class)) {
            return cb.equal(selectedPath.as(Integer.class), Integer.valueOf(value));
        } else if (selectedClazz.equals(long.class) || selectedClazz.equals(Long.class)) {
            return cb.equal(selectedPath.as(Long.class), Long.valueOf(value));
        } else if (selectedClazz.equals(BigDecimal.class)) {
            return cb.equal(selectedPath.as(BigDecimal.class), new BigDecimal(value));
        } else if (selectedClazz.equals(Date.class) || selectedClazz.equals(Timestamp.class)) {
            String strFrom = value;
            String strTo = value;
            Date from, to;

            //-- ranged date, e.g. : 2014-01-01..2014-01-31
            if (value.contains("..")) {
                String[] param = value.split("[\\.]{2,}"); //-- by regex two or more dots.
                strFrom = param[0];
                strTo = param[1];
            }

            //-- parse date using time offset given by client.
//            if (timeOffset != null && timeOffset.matches("[\\+\\-]?[\\d]{4}")) {
//                from = SDF.parse(strFrom + "T00:00:00" + timeOffset);
//                to = SDF.parse(strTo + "T23:59:59" + timeOffset);
//            } else
            {   //-- do without timeOffset
                from = SDF_WITHOUT_TIMEZONE.parse(strFrom + " 00:00:00");
                to = SDF_WITHOUT_TIMEZONE.parse(strTo + " 23:59:59");
            }
            return cb.between(selectedPath.as(Date.class), from, to);
        }

        else {
            LOGGER.warn("Unknown field type : {}, {}", fieldName, selectedClazz);
            return cb.equal(selectedPath.as(String.class), value);
        }
    }

    public <T> Specification<T> getRequestFilter() {
        final Map<String, String> filters = new HashMap<String, String>();

        if (queryParams != null) {
            String filterQuery = queryParams.get(QUERY_FILTER);
            //-- via filter query parameter
            //-- /dogs?filter="name::todd|city::denver|title::grand poobah�?
            if (filterQuery != null && filterQuery.length() > 0) {
                LOGGER.debug("filter : {}", filterQuery);
                try {
                    //-- split by | char.
                    String[] parameters = filterQuery.split("\\|");
                    for (String s : parameters) {
                        String tmp = s.trim();
                        //-- split key value by :: string.
                        String[] keypair = tmp.split("::");
                        if (keypair.length >= 2) {
                            filters.put(keypair[0], keypair[1]);
                            LOGGER.debug("filter by {}: {}", keypair[0], keypair[1]);
                        }
                    }
                } catch (Exception e) {
                    LOGGER.error("Failed to get filter query.", e);
                }
            } //-- via the rest of query parameters
            //-- /dogs?name=john&lname=cussack
            else {
                LOGGER.debug("No filter query, trying to scavenge available query parameters.");
                try {
                    for (String key : queryParams.keySet()) {
                        if (RESERVED_QUERY_PARAMETER.contains(key.toLowerCase())) {
                            continue;
                        }
                        String val = queryParams.get(key);
                        filters.put(key, val);
                        LOGGER.debug("filter by {}: {}", key, val);
                    }
                } catch (Exception e) {
                    LOGGER.error("Failed to scavenge filter query.", e);
                }
            }
        }

        Specification<T> spec = new Specification<T>() {
            @Override
            public Predicate toPredicate(Root<T> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                List<Predicate> predicates = new ArrayList<Predicate>();
                for (String fieldName : filters.keySet()) {
                    try {
                        predicates.add(
                                getSimplePredicate(root, cq, cb, fieldName, filters.get(fieldName))
                        );
                    } catch (Exception e) {
                        LOGGER.error("Failed to get simple predicate.", e);
                        throw new RuntimeException(e);
                    }
                }
                return cb.and(predicates.toArray(new Predicate[0]));
            }
        };
        return spec;
    }
}