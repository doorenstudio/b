package com.dooren.b.cmn.exception.errorcode;

public class ErrorCode {

    public static final Integer EXCEPTION=90;
    public static final Integer API_EXCEPTION=91;
    public static final Integer NOT_FOUND_EXCEPTION=92;
}
