package com.dooren.b.cmn.exception;

/**
 *
 * @author Daniel Joi Partogi Hutapea
 */
public class ExpressionUtilsException extends RuntimeException
{
    public ExpressionUtilsException()
    {
    }

    public ExpressionUtilsException(String message)
    {
        super(message);
    }

    public ExpressionUtilsException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public ExpressionUtilsException(Throwable cause)
    {
        super(cause);
    }

    public ExpressionUtilsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}