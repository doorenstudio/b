package com.dooren.b.cmn.repository;

import com.dooren.b.cmn.constant.RegionType;
import com.dooren.b.cmn.model.Region;
import com.dooren.b.cmn.generics.GenericRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RegionRepository extends GenericRepository<Region,Integer> {
    public List<Region> findByParentIdAndType(Integer parentId, RegionType type);
    public List<Region> findByType(RegionType type);

}
