package com.dooren.b.cmn.repository;

import com.dooren.b.cmn.constant.LookupType;
import com.dooren.b.cmn.generics.GenericRepository;
import com.dooren.b.cmn.model.Lookup;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LookupRepository extends GenericRepository<Lookup,Integer> {

    public List<Lookup> findByLookupType(LookupType lookupType);

}
