package com.dooren.b.cmn.constant;

public enum LookupType {
    REGION_TYPE,
    ACTIVITY_TYPE
}