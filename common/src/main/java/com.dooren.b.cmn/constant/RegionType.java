package com.dooren.b.cmn.constant;

public enum RegionType {
    PROVINCE,
    CITY,
    DISTRICT,
    VILLAGE
}
