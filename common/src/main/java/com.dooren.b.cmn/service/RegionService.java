package com.dooren.b.cmn.service;

import com.dooren.b.cmn.constant.RegionType;
import com.dooren.b.cmn.model.Region;
import com.dooren.b.cmn.repository.RegionRepository;
import com.dooren.b.cmn.generics.GenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RegionService extends GenericService<Region, Integer> {

    private RegionRepository repository;

    @Autowired
    public RegionService(RegionRepository repository) {
        super(repository);
        this.repository=repository;
    }

    public List<Region> findByParentIdAndType(Integer parentId, RegionType regionType){
        return repository.findByParentIdAndType(parentId, regionType);
    }

    public List<Region> findByType(RegionType regionType){
        return repository.findByType(regionType);
    }

}

