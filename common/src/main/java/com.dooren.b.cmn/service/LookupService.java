package com.dooren.b.cmn.service;

import com.dooren.b.cmn.constant.LookupType;
import com.dooren.b.cmn.generics.GenericService;
import com.dooren.b.cmn.model.Lookup;
import com.dooren.b.cmn.repository.LookupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LookupService extends GenericService<Lookup, Integer> {

    private LookupRepository repository;

    @Autowired
    public LookupService(LookupRepository repository) {
        super(repository);
        this.repository=repository;
    }

    public List<Lookup> findByLookupType(LookupType lookupType){
        return repository.findByLookupType(lookupType);
    }


}

