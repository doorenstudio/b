package com.dooren.b.cmn.rest;

import com.dooren.b.cmn.constant.LookupType;
import com.dooren.b.cmn.generics.GenericController;
import com.dooren.b.cmn.model.Lookup;
import com.dooren.b.cmn.service.LookupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(value = "/lookup")
public class LookupController extends GenericController<Lookup, Integer> {

    private LookupService service;

    @Autowired
    public LookupController(LookupService service) {
        super(service);
        this.service = service;
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/dummy")
    @ResponseBody
    public ResponseEntity<Lookup> dummy() {
        return new ResponseEntity<Lookup>(new Lookup(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/type/{type}")
    @ResponseBody
    public ResponseEntity<List<Lookup>> byType(@PathVariable("type") LookupType type) {
        List<Lookup> entities = service.findByLookupType(type);
        return new ResponseEntity<List<Lookup>>(entities, HttpStatus.OK);
    }


}
