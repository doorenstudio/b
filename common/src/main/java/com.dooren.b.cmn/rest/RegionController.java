package com.dooren.b.cmn.rest;

import com.dooren.b.cmn.constant.RegionType;
import com.dooren.b.cmn.generics.GenericController;
import com.dooren.b.cmn.model.Region;
import com.dooren.b.cmn.service.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(value = "/region")
public class RegionController extends GenericController<Region, Integer> {

    private RegionService service;

    @Autowired
    public RegionController(RegionService service) {
        super(service);
        this.service = service;
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/dummy")
    @ResponseBody
    public ResponseEntity<Region> dummy() {
        return new ResponseEntity<Region>(new Region(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/{type}/{parentId}")
    @ResponseBody
    public ResponseEntity<List<Region>> byType(@PathVariable("parentId") Integer parentId, @PathVariable("type") RegionType type) {
        List<Region> entities = service.findByParentIdAndType(parentId, type);
        return new ResponseEntity<List<Region>>(entities, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/type/{type}")
    @ResponseBody
    public ResponseEntity<List<Region>> byType(@PathVariable("type") RegionType type) {
        List<Region> entities = service.findByType(type);
        return new ResponseEntity<List<Region>>(entities, HttpStatus.OK);
    }

}
