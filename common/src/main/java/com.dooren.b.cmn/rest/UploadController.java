package com.dooren.b.cmn.rest;

import com.dooren.b.cmn.hostedfile.DropBoxHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.UUID;

@Controller
@RequestMapping(value = "/upload")
public class UploadController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UploadController.class.getName());

    @RequestMapping(
            method = RequestMethod.POST)
    public ResponseEntity uploadFile(MultipartHttpServletRequest files) {

            Iterator<String> itr= files.getFileNames();
        String fileName="";
        String url="";
            while (itr.hasNext()){
                String uploadedFileName= itr.next();
                MultipartFile multipartFile=files.getFile(uploadedFileName);
                UUID uuid = UUID.randomUUID();
                try {
                    File fileReal = multipartToFile(multipartFile);
                    String fileExt= fileReal.getName();
                    int lastDotIdx= fileExt.lastIndexOf(".");
                    fileExt=fileExt.substring(lastDotIdx);
                    fileName=uuid.toString()+fileExt;
                    DropBoxHelper.uploadFile(fileReal,"/"+fileName );
                    url = DropBoxHelper.createShareUrl("/"+fileName);
                } catch (Exception ex) {
                    LOGGER.error("Failed to upload image", ex);
                    return new ResponseEntity("Failed to upload file ", HttpStatus.BAD_REQUEST);
                }
            }
        HashMap<String,Object> response = new HashMap<>();
        response.put("fileName",fileName);
        response.put("url",url);
        return new ResponseEntity(response, HttpStatus.OK);
    }


    private File multipartToFile(MultipartFile multipart) throws IllegalStateException, IOException {
        File convFile = new File(multipart.getOriginalFilename());
        convFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(multipart.getBytes());
        fos.close();
        return convFile;
    }
}
