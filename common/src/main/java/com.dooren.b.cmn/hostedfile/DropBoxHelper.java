package com.dooren.b.cmn.hostedfile;

import com.dropbox.core.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class DropBoxHelper {


    public static final String ACESS_TOKEN="Hhf0rDdZYGsAAAAAAAAAQsMU3dtvbi_aHAdVTudLnz8QS7HkqTcqAecC43AtXyLQ";
    private static DbxClient client=null;
    private static DbxRequestConfig config= new DbxRequestConfig("propertio/1.0", Locale.getDefault().toString());

    public static DbxClient getClient(){
        if(client==null){
            client= new DbxClient(config,ACESS_TOKEN);
        }
        return client;
    }


    public static void uploadFile(File file,String remotePath)throws DoorenHelperException,DbxException{
        try {
            FileInputStream inputStream= new FileInputStream(file);
            DbxEntry.File fileUpload= getClient().uploadFile(remotePath, DbxWriteMode.add(),file.length(),inputStream);
            System.out.println("File uploaded to "+ remotePath);
            inputStream.close();
        } catch (FileNotFoundException e) {
            throw new DoorenHelperException();
        } catch (IOException e) {
            throw  new DoorenHelperException();
        }
    }

    public static DbxEntry getMetadata(String remotePath){
        try{
            DbxEntry dbxEntry= getClient().getMetadata(remotePath);
            return dbxEntry;
        } catch (DbxException e) {
            e.printStackTrace();
            return null;
        }

    }

    public List<DbxEntry> listFiles(String remotePath){
        try{
            DbxEntry.WithChildren children= getClient().getMetadataWithChildren(remotePath);
            return children.children;
        }catch (DbxException e){
            e.printStackTrace();
            return null;
        }

    }


    public static String createShareUrl(String remotePath){
        try {
            String dlUrl=getClient().createShareableUrl(remotePath);
            String rawUrl="";
            int lastIndexOf=dlUrl.lastIndexOf('?');
            rawUrl=dlUrl.substring(0,lastIndexOf);
            rawUrl+="?raw=1";
            return rawUrl;
        } catch (DbxException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void deleteFile(String remotePath){
        try {
            getClient().delete(remotePath);
        } catch (DbxException e) {
            e.printStackTrace();
        }
    }




}
