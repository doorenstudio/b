package com.dooren.b.cmn.filter;

import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter to enable Cross Origin Resource Sharing (CORS).
 *
 * @author fk
 */
@Component
public class CorsFilter implements Filter {
    static final String HTTP_HEADERS = ""
            + "accept, "
            + "accept-language, "
            + "authorization, "
            + "content-disposition, "
            + "content-encoding, "
            + "content-length, "
            + "content-range, "
            + "content-type, "
            + "if-match, "
            + "if-none-match, "
            + "oauth_consumer_key, "
            + "oauth_nonce, "
            + "oauth_signature, "
            + "oauth_signature_method, "
            + "oauth_timestamp, "
            + "oauth_version, "
            + "range, "
            + "user-time-offset, "
            + "x-authorization, "
            + "x-range, "
            + "x-requested-with, "
            +"Cache-Control,"
            + "";

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException
    {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        boolean allow = false;
        if (request.getMethod().equals("OPTIONS")) {
            //-- TODO : validate preflight here
            allow = true;
        } else {
            allow = true;
        }

        //-- set CORS headers
        if (allow) {
            response.setHeader("Access-Control-Allow-Origin", "*");
            //-- viable methods to query the resource in question
            response.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
            //-- how long the response to the preflight request can be cached for without sending another preflight request
            response.setHeader("Access-Control-Max-Age", "3600");
            //-- acceptable headers
            response.setHeader("Access-Control-Allow-Headers", HTTP_HEADERS);
            //-- exposable headers
            response.setHeader("Access-Control-Expose-Headers", HTTP_HEADERS);
            //-- acceptable headers
            response.setHeader("Access-Control-Allow-Credentials", "true");
        }

        if (request.getMethod().equals("OPTIONS")) {
            response.getWriter().print("OK");
            response.getWriter().flush();
        } else {
            chain.doFilter(req, res);
        }
    }

    public void init(FilterConfig filterConfig)
    {
    }

    public void destroy()
    {
    }
}