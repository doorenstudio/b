package com.dooren.b.cmn.generics;

import com.dooren.b.cmn.exception.NotFoundException;
import com.dooren.b.cmn.exception.errorcode.ErrorCode;
import com.dooren.b.cmn.util.RestQueryHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GenericController<T extends Object, ID extends Serializable> {

    private Logger logger = LoggerFactory.getLogger(getClass().getName());

    protected final GenericService<T, ID> service;

    @Autowired
    private HttpServletRequest request;

    public GenericController(GenericService<T, ID> service) {
        this.service = service;
    }

    //create
    @RequestMapping(
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    public ResponseEntity<T> create(@RequestBody T entity) {
        T saved = service.save(entity);
        return new ResponseEntity<T>(saved, HttpStatus.OK);
    }

    //update
    @RequestMapping(
            method = RequestMethod.PUT,
            value = "/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<T> update(@RequestBody T entity, @PathVariable ID id) {
        T saved = service.update(entity, id);
        return new ResponseEntity<T>(saved, HttpStatus.OK);
    }

    //delete
    //byId
    @RequestMapping(
            method = RequestMethod.DELETE,
            value = "/{id}"
    )
    @ResponseBody
    public ResponseEntity<Map<String, Object>> deleteById(@PathVariable ID id) {
        Boolean deleted = service.deleteById(id);
        Map<String, Object> mapResponse = new HashMap<String, Object>();
        mapResponse.put("deleted", deleted);
        return new ResponseEntity<Map<String, Object>>(mapResponse, HttpStatus.OK);
    }

    //delete
    //byObject
    @RequestMapping(
            method = RequestMethod.DELETE
    )
    @ResponseBody
    public ResponseEntity<Map<String, Object>> delete(@RequestBody T entity) {
        Boolean deleted = service.delete(entity);
        Map<String, Object> mapResponse = new HashMap<String, Object>();
        mapResponse.put("deleted", deleted);
        return new ResponseEntity<Map<String, Object>>(mapResponse, HttpStatus.OK);
    }

    //find
    //byId
    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE,
            value = "/{id}"
    )
    @ResponseBody
    public ResponseEntity<T> findOne(@PathVariable ID id) {
        T entity = service.findById(id);
        if (entity == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_EXCEPTION, "Cannot found specified ID");
        }
        return new ResponseEntity<T>(entity, HttpStatus.OK);

    }

    //find
    //bySpecification
    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    public ResponseEntity<Map<String, Object>> find() throws Exception {
        //get query helper to check request
        logger.debug("processing get request");
        RestQueryHelper queryHelper = new RestQueryHelper(getHeaders(), getQueryParams());
        Specification<T> specz = queryHelper.getRequestFilter();
        Pageable paging = queryHelper.getRequestRangeAndSort();
        Page<T> page = service.find(specz, paging);
        List<T> list = page.getContent();
        Map<String, Object> mapResponse = new HashMap<String, Object>();
        mapResponse.put("entities", list);
        mapResponse.put("count", list.size());

        return new ResponseEntity<Map<String, Object>>(mapResponse, HttpStatus.OK);
    }

    /**
     * retrieve header into a map
     *
     * @return
     */
    protected Map<String, String> getHeaders() {
        Map<String, String> headers = new HashMap<>();
        Enumeration en = request.getHeaderNames();
        while (en.hasMoreElements()) {
            String k = (String) en.nextElement();
            String v = (String) request.getHeader(k);
            headers.put(k, v);

        }
        return headers;
    }

    /**
     * retrieve query params into a map
     *
     * @return
     */
    protected Map<String, String> getQueryParams() {
        Map<String, String> params = new HashMap<String, String>();
        Enumeration en = request.getParameterNames();
        while (en.hasMoreElements()) {
            String k = (String) en.nextElement();
            String v = request.getParameter(k);
            params.put(k, v);
        }
        return params;
    }

}
