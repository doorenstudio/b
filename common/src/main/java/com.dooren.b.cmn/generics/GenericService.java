package com.dooren.b.cmn.generics;

import com.dooren.b.cmn.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

public abstract class GenericService<T extends Object, ID extends Serializable> {

    private Logger logger = LoggerFactory.getLogger(getClass().getName());

    private GenericRepository<T, ID> repository;

    public GenericService(GenericRepository<T, ID> repository) {
        this.repository = repository;
    }

    //create
    @Transactional
    public T save(T object) {
        return repository.save(object);
    }

    //update
    @Transactional
    public T update(T object, ID id) {
        if (!repository.exists(id)) {
            throw new NotFoundException(1, "Cannot find entity with id " + id);
        }
        repository.save(object);
        return object;
    }

    //delete
    //by id
    @Transactional
    public Boolean deleteById(ID id) {
        boolean deleted = false;
        try {
            repository.delete(id);
            deleted = true;
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return deleted;
    }

    //delete
    //by object
    @Transactional
    public Boolean delete(T obj) {
        boolean deleted = false;
        try {
            repository.delete(obj);
            deleted = true;
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return deleted;
    }

    //find
    //byId
    public T findById(ID id) {
        T result = repository.findOne(id);
        if (result == null) {
            throw new NotFoundException(99, "Cannot find specified entitiy with id :" + id);
        }

        return repository.findOne(id);
    }

    //find
    //bySpecification
    public List<T> find(Specification<T> spec) {
        return repository.findAll(spec);

    }

    //find
    //bySpecification and page
    public Page<T> find(Specification<T> spec, Pageable pageable) {
        return repository.findAll(spec, pageable);
    }

    //find
    // All
    public List<T> findAll() {
        return repository.findAll();
    }

}
