package com.dooren.b.cmn.model;

import com.dooren.b.cmn.constant.LookupType;
import com.dooren.b.cmn.generics.AuditableEntity;

import javax.persistence.*;

@Entity
@Table(name = "m_lookups")
public class Lookup extends AuditableEntity {

    @Id
    @Column(name = "lookup_id", columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(name = "lookup_type", nullable = false)
    @Enumerated(EnumType.STRING)
    private LookupType lookupType;

    @Column(name="lookup_key", unique = true, nullable = false, length = 50)
    private String lookupKey;

    @Column(name="lookup_value", nullable = false, length = 50)
    private String lookupValue;

    @Column(name="status")
    private boolean status;

    public Lookup() {
    }

    public Lookup(LookupType lookupType, String lookupKey, String lookupValue, boolean status) {
        this.lookupType = lookupType;
        this.lookupKey = lookupKey;
        this.lookupValue = lookupValue;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public LookupType getLookupType() {
        return lookupType;
    }

    public String getLookupKey() {
        return lookupKey;
    }

    public String getLookupValue() {
        return lookupValue;
    }

    public boolean isStatus() {
        return status;
    }

    public void setLookupType(LookupType lookupType) {
        this.lookupType = lookupType;
    }

    public void setLookupKey(String lookupKey) {
        this.lookupKey = lookupKey;
    }

    public void setLookupValue(String lookupValue) {
        this.lookupValue = lookupValue;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
