package com.dooren.b.cmn.model;

import com.dooren.b.cmn.constant.RegionType;
import com.dooren.b.cmn.generics.AuditableEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "m_regions")
public class Region extends AuditableEntity {

    @Id
    @Column(name = "region_id", columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(name = "region_name",nullable = false,length = 100)
    private String name;

    @Column(name = "region_type", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    private RegionType type;

    @Column(name="is_active")
    private boolean active;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id", nullable = true, columnDefinition = "int", referencedColumnName = "region_id")
    @JsonIgnore
    private Region parent;

    @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Region> child;

    public Region() {
    }

    public Region(String name, RegionType type, boolean active) {
        this.name = name;
        this.type = type;
        this.active = active;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RegionType getType() {
        return type;
    }

    public void setType(RegionType type) {
        this.type = type;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Region getParent() {
        return parent;
    }

    public void setParent(Region parent) {
        this.parent = parent;
    }

    public List<Region> getChild() {
        return child;
    }

    public void setChild(List<Region> child) {
        this.child = child;
    }
}
